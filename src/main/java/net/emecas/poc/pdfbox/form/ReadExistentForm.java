package net.emecas.poc.pdfbox.form;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDNonTerminalField;

public class ReadExistentForm {
	
	String complement;

	public ReadExistentForm(String complement) {
		this.complement = complement;
	}
	
	public static void main(String[] args) {
		
		ReadExistentForm  ref = new ReadExistentForm("-CO-VoterRegFormEnglish"); 
		ref.process();
		
	}

	public void process()  {
		try (PDDocument document = PDDocument.load(new File("base"+complement+".pdf"))) {
			//PDPage page = document.getPages().iterator().next();
			
			PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();
			
			List<PDField> pdfFieldsList = acroForm.getFields();
			
			pdfFieldsList.forEach( f -> System.out.println( f.toString() +" "+ f.getAlternateFieldName()));
			
			
			PDDocumentCatalog docCatalog = document.getDocumentCatalog();
			PDAcroForm acroForm_ = docCatalog.getAcroForm();
			List<PDField> fields = acroForm_.getFields();
			
			System.out.println(fields.size() + " top-level fields were found on the form");

			for (PDField field : fields) {
				processField(field, "|--", field.getPartialName());
			}


		} catch (InvalidPasswordException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	 private void processField(PDField field, String sLevel, String sParent) throws IOException
	    {
	        String partialName = field.getPartialName();
	        
	        if (field instanceof PDNonTerminalField)
	        {
	            if (!sParent.equals(field.getPartialName()))
	            {
	                if (partialName != null)
	                {
	                    sParent = sParent + "." + partialName;
	                }
	            }
	            System.out.println(sLevel + sParent);

	            for (PDField child : ((PDNonTerminalField)field).getChildren())
	            {
	                processField(child, "|  " + sLevel, sParent);
	            }
	        }
	        else
	        {
	            String fieldValue = field.getValueAsString();
	            StringBuilder outputString = new StringBuilder(sLevel);
	            outputString.append(sParent);
	            if (partialName != null)
	            {
	                outputString.append(".").append(partialName);
	            }
	            outputString.append(" = ").append(fieldValue);
	            outputString.append(getCoordinates(field));
	            outputString.append(",  type=").append(field.getClass().getName());
	            System.out.println(outputString);
	        }
	    }

	private String getCoordinates(PDField field) {
		StringBuilder outputString = new StringBuilder();
		if( (field.getWidgets() != null) && 
		    (field.getWidgets().get(0) != null) && 
		    (field.getWidgets().get(0).getRectangle()!=null)){
		    PDRectangle rect = field.getWidgets().get(0).getRectangle();
		    outputString.append(" x = "+rect.getLowerLeftX());
		    outputString.append(" y = "+rect.getLowerLeftY());
		    outputString.append(" width = "+rect.getWidth());
		    outputString.append(" height = "+rect.getWidth());
		}
		return outputString.toString();
	}


}
