/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.emecas.poc.pdfbox.state.va;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

import com.opencsv.CSVReader;

/**
 * An example of creating an AcroForm and a form field from scratch.
 * 
 * The form field is created with properties similar to creating a form with
 * default settings in Adobe Acrobat.
 * 
 */
public final class VoteRegistrationFillerFont {
	
	String csvFile = "fields";
	String complement ="-VA-NVRA-1-04-16";
	
	List<Field> values; 
	
	class Field{
		public int x;
		public int y;
		public int lengt;
		public String value;
		public Field(int x, int y, int length,String value) {
			super();
			this.x = x;
			this.y = y;
			this.lengt =  length;
			this.value = value;
		}
		@Override
		public String toString() {
			return "Field [x=" + x + ", y=" + y + ", lengt=" + lengt + ", value=" + value + "]";
		}
	
		
	}
	
	private VoteRegistrationFillerFont() {
		
		values = new ArrayList<Field>();

        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(csvFile+complement+".csv"));
            String[] line;
            reader.readNext();
            while ((line = reader.readNext()) != null) {
                //System.out.println("field [length= " + line[5] + ", x= " + line[6] + ", y= " + line[7] + " , testData=" + line[12] + "]");
                if( !line[5].isEmpty() && !line[6].isEmpty() && !line[7].isEmpty() && !line[12].isEmpty() ){
                	
                	Field f =  new Field(Integer.parseInt(line[6]),Integer.parseInt(line[7]),Integer.parseInt(line[5]),line[12]);
                	values.add(f);
                	System.out.println(""+f);
                	
                	
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public static void main(String[] args) throws IOException {
		VoteRegistrationFillerFont vrf = new VoteRegistrationFillerFont();
		vrf.filler();
	}
	
	void filler() throws InvalidPasswordException, IOException{
		// Create a new document with an empty page.
		// try (PDDocument document = new PDDocument())

		// Load existent PDF
		try (PDDocument document = PDDocument.load(new File("base"+complement+".pdf"))) {
			// PDPage page = new PDPage(PDRectangle.A4);
			// document.addPage(page);

			PDPage page = document.getPages().iterator().next();

			// Adobe Acrobat uses Helvetica as a default font and
			// stores that under the name '/Helv' in the resources dictionary
			PDFont font = PDType1Font.COURIER;
			PDResources resources = new PDResources();
			resources.put(COSName.getPDFName("Helv"), font);

			// Add a new AcroForm and add that to the document
			PDAcroForm acroForm = new PDAcroForm(document);
			document.getDocumentCatalog().setAcroForm(acroForm);

			// Add and set the resources and default appearance at the form
			// level
			acroForm.setDefaultResources(resources);

			// Acrobat sets the font size on the form level to be
			// auto sized as default. This is done by setting the font size to
			// '0'
			String defaultAppearanceString = "/Helv 0 Tf 0 g";
			acroForm.setDefaultAppearance(defaultAppearanceString);

			// Add a form field to the form.
			PDTextField textBox = new PDTextField(acroForm);
			textBox.setPartialName("SampleField");

			// Acrobat sets the font size to 12 as default
			// This is done by setting the font size to '12' on the
			// field level.
			// The text color is set to blue in this example.
			// To use black, replace "0 0 1 rg" with "0 0 0 rg" or "0 g".
			defaultAppearanceString = "/Helv 10 Tf 0 0 1 rg";
			textBox.setDefaultAppearance(defaultAppearanceString);

			// add the field to the acroform
			acroForm.getFields().add(textBox);

			Iterator<Field> ite = values.iterator();
				
			while(ite.hasNext()){
				Field f = ite.next();

				PDTextField textBox1 = new PDTextField(acroForm);
				textBox1.setPartialName("SampleField" + f.x + f.y);

				defaultAppearanceString = "/Helv 10 Tf 0 0 1 rg";
				textBox1.setDefaultAppearance(defaultAppearanceString);

				acroForm.getFields().add(textBox1);

				PDAnnotationWidget widget1 = textBox1.getWidgets().get(0);
				PDRectangle rect1 = new PDRectangle(f.x, f.y, f.lengt==1?10:(int)(5.56*f.lengt), 10);
				widget1.setRectangle(rect1);
				widget1.setPage(page);

				widget1.setPrinted(true);

				// Add the widget annotation to the page
				page.getAnnotations().add(widget1);

				// set the field value
				textBox1.setValue(f.value);
			}

			document.save("FilledForm"+complement+".pdf");
		}
	}
}
