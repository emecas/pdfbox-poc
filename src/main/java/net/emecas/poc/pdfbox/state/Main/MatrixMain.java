package net.emecas.poc.pdfbox.state.Main;

import java.util.Arrays;
import java.util.List;

import net.emecas.poc.pdfbox.state.CreateGenericMatrixForm;



public class MatrixMain {
	
	static String[] complements = {"-VA-NVRA-1-04-16","-CO-VoterRegFormEnglish","-TX-VR-WITH-RECEIPT"};
	
	public static void main(String[] args) {
		
		List<String> complementsList = (List<String>) Arrays.asList(complements);
		
		complementsList.forEach( com -> {CreateGenericMatrixForm gcm = new CreateGenericMatrixForm(com); gcm.matrix(); } );
		
	}

}
