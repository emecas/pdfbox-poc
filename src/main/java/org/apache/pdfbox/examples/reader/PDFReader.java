package org.apache.pdfbox.examples.reader;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.text.PDFTextStripper;

public class PDFReader{
 public static void main(String args[]) {
     PDFTextStripper pdfStripper = null;
     PDDocument pdDoc = null;
     COSDocument cosDoc = null;
     File file = new File("base.pdf");
     try {
         // PDFBox 2.0.8 require org.apache.pdfbox.io.RandomAccessRead 
         RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
         PDFParser parser = new PDFParser(randomAccessFile);

         //PDFParser parser = new PDFParser(new FileInputStream(file));
         parser.parse();
         cosDoc = parser.getDocument();
         pdfStripper = new PDFTextStripper();
         pdDoc = new PDDocument(cosDoc);
         pdfStripper.setStartPage(1);
         pdfStripper.setEndPage(5);
         String parsedText = pdfStripper.getText(pdDoc);
         
         System.out.println(parsedText);
         
         PDDocumentInformation docInf = pdDoc.getDocumentInformation();
        
         System.out.println(pdDoc.getDocumentInformation());
         System.out.println(docInf.getMetadataKeys());
         System.out.println(docInf.getCOSObject().keySet());
         
         
         
         
         //PDMetadata mediaData = pdDoc.getPages().get(0).getMetadata();
         //System.out.println(mediaData.getMetadata().toString());
         
     } catch (IOException e) {
         e.printStackTrace();
     } 
 }
}
